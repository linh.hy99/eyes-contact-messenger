package com.socket;

import java.io.*;
import java.util.Base64;

class ClientInfo implements Serializable {
    public String IP = "";
    public int port = 1;
    public String username = "";
    private static final long serialVersionUID = 1L;

    public ClientInfo(String _IP, int _port, String _username) {
        IP = _IP;
        port = _port;
        username = _username;
    }

    public static Object fromString(String s) {
        try {
            byte[] data = Base64.getDecoder().decode(s);
            ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
            Object o = ois.readObject();
            ois.close();
            return o;
        }
        catch (Exception e){
            return null;
        }
    }

    public static String toString(Serializable o) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(o);
            oos.close();
            return Base64.getEncoder().encodeToString(baos.toByteArray());
        }
        catch (Exception e) {
            return "";
        }
    }
}
