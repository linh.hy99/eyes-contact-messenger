package com.socket;

import com.ui.LoginFrame;
import com.ui.ChatFrame;
import java.io.*;
import java.net.*;
import javax.swing.JOptionPane;
import javax.swing.JFileChooser;

class ServerThread extends Thread {

    public SocketP2P server = null;
    public Socket socket = null;
    public int ID = -1;
    public String IP;
    public String username = "";
    public ObjectInputStream  streamIn  = null;
    public ObjectOutputStream streamOut = null;
    public ChatFrame ui;

    public ServerThread(SocketP2P _server, Socket _socket) {
        super();
        server = _server;
        socket = _socket;
        ID     = socket.getPort();
        ui = _server.ui;
        IP = socket.getInetAddress().getHostAddress();
    }

    public ClientInfo getClientInfo() {
        return new ClientInfo(IP, ID, username);
    }

    public void send(Message msg) {
        try {
            streamOut.writeObject(msg);
            streamOut.flush();
        }
        catch (IOException ex) {
            System.out.println("Exception [SocketClient : send(...)]");
        }
    }

    @SuppressWarnings("deprecation")
    public void run() {
        while (true) {
            try {
                Message msg = (Message) streamIn.readObject();
                server.handle(ID, msg);
            }
            catch (Exception ioe) {
                System.out.println(ID + " ERROR reading: " + ioe.getMessage());
                server.remove(ID);
                stop();
            }
        }
    }

    public void open() throws IOException {
        streamOut = new ObjectOutputStream(socket.getOutputStream());
        streamOut.flush();
        streamIn = new ObjectInputStream(socket.getInputStream());
    }

    public void close() throws IOException {
        if (socket != null)    socket.close();
        if (streamIn != null)  streamIn.close();
        if (streamOut != null) streamOut.close();
    }
}

public class SocketP2P implements Runnable {
    public ServerThread clients[];
    public ServerSocket server = null;
    public Thread thread = null;
    public int clientCount = 0;
    public int port;
    public ChatFrame ui;
    private int maxPort = 65535;
    private int minPort = 13001;
    private int counter = 0;
    public boolean isRunning = false;

    public SocketP2P(ChatFrame frame, int Port) {
        clients = new ServerThread[5000];
        ui = frame;
        port = Port;
        try {
            server = new ServerSocket(port);
            port = server.getLocalPort();
            start();
        }
        catch(IOException ioe) {
            ui.RetryStart();
        }
    }

    public void run() {
        isRunning = true;
        while (thread != null && isRunning) {
            try {
                IncomingConnection(server.accept());
            }
            catch(Exception ioe) {
                ui.RetryStart();
            }
        }
    }

    public void IncomingConnection(Socket socket) {
        String socketIP = socket.getInetAddress().toString();
        int socketPort = socket.getPort();
        for (int i=0; i < clientCount; i++){
            if (clients[i].IP.equals(socketIP) && clients[i].ID == socketPort) {
                System.out.println("23jkhtbnwei");
                return;
            }
        }
        addThread(socket);
    }

    private void addThread(Socket socket) {
        clients[clientCount] = new ServerThread(this, socket);
        try {
            clients[clientCount].open();
            clients[clientCount].start();
            clientCount++;
        }
        catch (IOException ioe) {
        }
    }

    public void ConnectTo(ClientInfo clientInfo) {
        try {
            InetSocketAddress endpoint = new InetSocketAddress(clientInfo.IP, clientInfo.port);
            Socket socket = new Socket();
            socket.connect(endpoint);
            addThread(socket);
            clients[clientCount - 1].username = clientInfo.username;
            clients[clientCount - 1].send(new Message("info", ui.username, "OK", clientInfo.username));
            ui.model.addElement(clientInfo.username);
        }
        catch (UnknownHostException e) {
            System.out.println("Unknow host");
            System.out.println(e);
        }
        catch (IOException e) {
            System.out.println("Can not connect to " + clientInfo.username);
        }
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }

    @SuppressWarnings("deprecation")
    public void stop() {
        isRunning = false;
        if (thread != null) {
            thread.stop();
            thread = null;
        }
        try {
            server.close();
        }
        catch (IOException ex) { }

    }

    private int findClient(int ID) {
        for (int i = 0; i < clientCount; i++)
            if (clients[i].ID == ID)
                return i;
        return -1;
    }

    public ServerThread findUserThread(String usr) {
        for (int i = 0; i < clientCount; i++)
            if (clients[i].username.equals(usr))
                return clients[i];
        return null;
    }

    public synchronized void handle(int ID, Message msg) {
        if (msg.type.equals("info")) {
            int pos = findClient(ID);
            if (pos > -1) {
                clients[findClient(ID)].username = msg.sender;
                ui.model.addElement(msg.sender);
            }
        } else if (msg.type.equals("test")) {
            clients[findClient(ID)].send(new Message("test", ui.username, "OK", msg.sender));
        } else if (msg.type.equals("message")) {
            if (msg.recipient.equals("All")){
                ui.AddMessage("All", msg.sender + ": " + msg.content);
            }
            else
                ui.AddMessage(msg.sender, msg.sender + ": " + msg.content);
        } else if (msg.type.equals("upload_req")){
            ui.AddMessage(msg.sender, "Requesting \"" + msg.content + "\" file from " + msg.sender + ".");
            if (JOptionPane.showConfirmDialog(ui, ("<html>Accept <b>" + msg.content + "</b> from <b>"+msg.sender+"</b> ?</html>")) == 0) {
                JFileChooser jf = new JFileChooser();
                jf.setSelectedFile(new File(msg.content));
                int returnVal = jf.showSaveDialog(ui);
                String saveTo = jf.getSelectedFile().getPath();
                if(saveTo != null && returnVal == JFileChooser.APPROVE_OPTION){
                    Download dwn = new Download(saveTo, ui, msg.sender, msg.content);
                    Thread t = new Thread(dwn);
                    t.start();
                    send(new Message("upload_res", ui.username, ("" + dwn.port), msg.sender));
                }
                else {
                    send(new Message("upload_res", ui.username, "NO", msg.sender));
                    ui.AddMessage(msg.sender, "Declined \"" + msg.content + "\" file!");
                }
            }
            else {
                send(new Message("upload_res", ui.username, "NO", msg.sender));
                ui.AddMessage(msg.sender, "Declined \"" + msg.content + "\" file!");
            }
        } else if (msg.type.equals("upload_res")) {
            if (!msg.content.equals("NO")) {
                ui.AddMessage(msg.sender, "Uploading \"" + ui.file.getName() + "\" file to " + msg.sender + ".");
                int port  = Integer.parseInt(msg.content);
                String addr = msg.sender;
                ui.jButtonFile.setEnabled(false);
                try {
                    Upload upl = new Upload(findUserThread(addr).IP, port, ui.file, ui, msg.sender);
                    Thread t = new Thread(upl);
                    t.start();
                }
                catch (Exception ex) {
                    ui.AddMessage(msg.sender, "Upload \"" + ui.file.getName() + "\" file Failure.");
                }
                finally{
                    ui.jButtonFile.setEnabled(true);
                    ui.file = null;
                }
            }
            else
                ui.AddMessage(msg.sender, "Send \"" + ui.file.getName() + "\" file request rejected.");
        } else if (msg.type.equals("signout")) {
            remove(ID);
            ui.remove(msg.sender);
        }
    }

    public void Announce(String type, String sender, String content) {
        Message msg = new Message(type, sender, content, "All");
        for(int i = 0; i < clientCount; i++)
            clients[i].send(msg);
    }

    public void Announce(String type, String sender, ClientInfo clientInfo) {
        Message msg = new Message(type, sender, clientInfo, "All");
        for(int i = 0; i < clientCount; i++)
            clients[i].send(msg);
    }

    public void SendUserList(String toWhom) {
        for(int i = 0; i < clientCount; i++) {
            findUserThread(toWhom).send(new Message("newuser", ui.username, findUserThread(clients[i].username).getClientInfo(), toWhom));
        }
    }

    public void send(Message msg) {
        if (msg.recipient.equals("All")) {
            if (msg.type.equals("upload_req")) {// || msg.type.equals("upload_res"))
                JOptionPane.showMessageDialog(ui, "<html>Upload file to <b>All</b> is forbidden!</html>", "Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            else
                for (int i = 0; i < clientCount; i++)
                    clients[i].send(msg);
        }
        else {
            ServerThread target = findUserThread(msg.recipient);
            if (target != null)
                target.send(msg);
        }
        if (msg.type.equals("upload_req"))
            ui.AddMessage(msg.recipient, "Requesting \"" + msg.content + "\" file to " + msg.recipient + ".");
        else if (msg.type.equals("upload_res"));
        else
            ui.AddMessage(msg.recipient, "You: " + msg.content);
    }

    @SuppressWarnings("deprecation")
    public synchronized void remove(int ID) {
        int pos = findClient(ID);
        if (pos >= 0) {
            ServerThread toTerminate = clients[pos];
            if (pos < clientCount-1)
                for (int i = pos+1; i < clientCount; i++)
                    clients[i-1] = clients[i];
            clientCount--;
            try {
                  toTerminate.close();
            }
            catch(IOException ioe) {
            }
//            toTerminate.stop();
        }
    }
}
