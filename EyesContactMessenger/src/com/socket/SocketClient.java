package com.socket;

import com.ui.LoginFrame;
import com.ui.ChatFrame;
import java.io.*;
import java.net.*;

public class SocketClient implements Runnable {
    public int port;
    public String serverAddr;
    public Socket socket;
    public LoginFrame ui_login;
    public ChatFrame ui_chat;
    public ObjectInputStream In;
    public ObjectOutputStream Out;
    public boolean isRunning = false;

    public SocketClient(LoginFrame frame) throws IOException {
        ui_login = frame;
        this.serverAddr = ui_login.serverAddr;
        this.port = ui_login.port;
        InetAddress a = InetAddress.getByName(serverAddr);
        socket = new Socket(a, port);
        socket.setSoTimeout(5000);
        Out = new ObjectOutputStream(socket.getOutputStream());
        Out.flush();
        In = new ObjectInputStream(socket.getInputStream());
    }

    @Override
    public void run() {
        isRunning = true;
        while (isRunning) {
            try {
                Message msg = (Message) In.readObject();
                System.out.println("Incoming : " + msg.toString());
                if (msg.type.equals("test")) {
                } else if (msg.type.equals("newuser")) {
                    ClientInfo clientInfo = (ClientInfo) ClientInfo.fromString(msg.content);
                    if (!clientInfo.username.equals(ui_login.username)) {
                        for (int i = 0; i < ui_chat.model.getSize(); i++)
                            if (ui_chat.model.getElementAt(i).equals(clientInfo.username)) {
                                ui_chat.p2p.remove(clientInfo.port);
                                ui_chat.p2p.ConnectTo(clientInfo);
                                return;
                            }
                        ui_chat.p2p.ConnectTo(clientInfo);
                    }
                } else if (msg.type.equals("login")) {
                    if (msg.content.equals("TRUE")) {
                        login();
                    } else
                        ui_login.loginFail(0);
                } else if (msg.type.equals("signup")) {
                    if (msg.content.equals("TRUE")) {
                        login();
                    } else
                        ui_login.loginFail(1);
                } else {
                    System.out.println("Unknow msg.type: " + msg.type);
                }
            }
            catch (EOFException ex) {
                isRunning = false;
            }
            catch (SocketTimeoutException ex) {}
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void login() {
        ui_login.setVisible(false);
        ui_chat = new ChatFrame(this);
    }

    public void send(Message msg) {
        try {
            if (msg.recipient.equals("SERVER")) {
                Out.writeObject(msg);
                Out.flush();
                System.out.println("Outgoing : " + msg.toString());
            }
        } catch (IOException ex) {
            System.out.println("Exception SocketClient send()");
        }
    }

    public void closeThread(Thread t){
        t = null;
    }
}
