package com.socket;

import com.ui.ChatFrame;
import java.io.*;
import java.net.*;

public class Download implements Runnable{

    public ServerSocket server;
    public Socket socket;
    public int port;
    public String saveTo = "";
    public InputStream In;
    public FileOutputStream Out;
    public ChatFrame ui;
    public String source;
    public String filename;

    public Download (String saveTo, ChatFrame ui, String _source, String _filename) {
        try {
            server = new ServerSocket(0);
            port = server.getLocalPort();
            this.saveTo = saveTo;
            this.ui = ui;
            source = _source;
            filename = _filename;
        }
        catch (IOException ex) {}
    }

    @Override
    public void run() {
        try {
            socket = server.accept();
            ui.AddMessage(source, "Downloading \"" + filename + "\" file from " + source +".");

            In = socket.getInputStream();
            Out = new FileOutputStream(saveTo);

            byte[] buffer = new byte[1024];
            int count;

            while((count = In.read(buffer)) >= 0){
                Out.write(buffer, 0, count);
            }

            Out.flush();
            ui.AddMessage(source, "Download \"" + filename + "\" complete to \"" + saveTo + "\" complete.");

            if(Out != null){ Out.close(); }
            if(In != null){ In.close(); }
            if(socket != null){ socket.close(); }
        }
        catch (Exception ex) { }
    }
}